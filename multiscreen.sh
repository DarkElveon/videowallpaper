#!/bin/bash

if [ $# -lt 1 ]; then
  echo "provide a video path"
  exit 0
fi

VIDEO_FILE=$1
SCREENS=$(xwininfo -root -children | grep Schreibtisch | awk '{$1=$1};1' | cut -d " " -f 1)

for SCREEN_ID in $SCREENS; do
    SCREEN_WIDTH=$(xwininfo -id $SCREEN_ID | grep "Width" | awk '{$1=$1};1' | cut -d " " -f 2)
    SCREEN_HEIGHT=$(xwininfo -id $SCREEN_ID | grep "Height" | awk '{$1=$1};1' | cut -d " " -f 2)
    SCREEN_OFFSET=$(xwininfo -id $SCREEN_ID | grep "Corners" | awk '{$1=$1};1' | cut -d " " -f 4)
    GEOMETRY=$(echo "$SCREEN_WIDTH"x"$SCREEN_HEIGHT""$SCREEN_OFFSET")
    ./screen.sh $VIDEO_FILE $SCREEN_ID $GEOMETRY &
done
