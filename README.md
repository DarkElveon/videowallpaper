# VideoWallpaper

Simple video wallpaper for X11.

This project is based on the project https://github.com/valgusk/windesktop.
Many thanks to the original author.

This is basically an attempt to improve/extend the featureset of the project, such as:
  
  * support for multiscreen setups
  * identify the resolution of each monitor individually
  * start video wallpaper for each monitor independently
  * systray icon

## Installation

### Arch

```shell
sudo pacman -S xorg-xprop xorg-xwininfo wmctrl socat mpv
git clone https://gitlab.com/DarkElveon/videowallpaper.git
```

### Ubuntu/Mint

```shell
sudo apt install x11-utils wmctrl socat mpv
git clone https://gitlab.com/DarkElveon/videowallpaper.git
```

## Usage

  * ```./multiscreen.sh <video-file>``` - start video wallpaper on every screen
  * ```./windesktop.sh <video-file> <screen_id>``` - start video wallpaper on single screen
  * ```./windesktop.sh <video-file> <screen_id> <mpv_geometry>``` - start video wallpaper on single screen with custom geometry
  * ```killall windesktop.sh``` - stop all instances


## Other applications (future work)

  * nothing yet
