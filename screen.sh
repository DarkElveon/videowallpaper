#!/bin/bash

if [ $# -lt 1 ]; then
  echo "provide parameter <video_path> <screen_id> [<mpv_geometry>]"
  exit 0
fi

VIDEO_FILE=$1

if [ $# -eq 1 ]; then
  echo "Select the screen"
  SCREEN_ID=$(xwininfo | grep 'Window id' | cut -d ' ' -f 4)
else
  SCREEN_ID=$2
fi

if [ $# -lt 3 ]; then
  SCREEN_WIDTH=$(xwininfo -id $SCREEN_ID | grep "Width" | awk '{$1=$1};1' | cut -d " " -f 2)
  SCREEN_HEIGHT=$(xwininfo -id $SCREEN_ID | grep "Height" | awk '{$1=$1};1' | cut -d " " -f 2)
  SCREEN_OFFSET=$(xwininfo -id $SCREEN_ID | grep "Corners" | awk '{$1=$1};1' | cut -d " " -f 4)
  GEOMETRY=$(echo "$SCREEN_WIDTH"x"$SCREEN_HEIGHT""$SCREEN_OFFSET")
  echo $GEOMETRY
else
  GEOMETRY=$3
fi

FILL_SCREEN=true

if [ "$FILL_SCREEN" = true ]; then
  echo "preserve aspect ratio"
  mpv --hwdec=auto --fs --no-border --panscan=1.0 --geometry=$GEOMETRY --really-quiet --no-osc --loop --no-audio $VIDEO_FILE --input-ipc-server=/tmp/mpvsocket &
else
  echo "ignore aspect ratio"
  mpv --hwdec=auto --fs --no-border --geometry=$GEOMETRY --really-quiet --no-osc --loop --no-audio $VIDEO_FILE --input-ipc-server=/tmp/mpvsocket &
fi

pid=$!
echo "PID: $pid" 

if ! [ $pid ]; then  
  echo "PID is empty! exiting!"
  exit 0
fi

clean_up () {
  echo "exiting"
  kill $pid

  exit
}

trap clean_up SIGHUP SIGINT SIGTERM

echo "waiting for window..."

until $(echo wmctrl -lp)|grep " $pid "; do :; done

WINDOW_ID=$($(echo wmctrl -lp)|grep " $pid "|sed 's/.*\(0x\w*\).*/\1/')

until $(echo xwininfo -id $WINDOW_ID)|grep "IsViewable"; do :; done

echo "window: $WINDOW_ID, desktop handler: $SCREEN_ID, PID: $pid"

./windesktop $WINDOW_ID $SCREEN_ID

echo "started" 

wait $pid
